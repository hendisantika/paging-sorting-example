package com.hendisantika.pagingsortingexample.controller;

import com.hendisantika.pagingsortingexample.model.Client;
import com.hendisantika.pagingsortingexample.model.Pager;
import com.hendisantika.pagingsortingexample.repository.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : paging-sorting-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/01/18
 * Time: 18.01
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class ClientController {
    private static final int BUTTONS_TO_SHOW = 3;

    private static final int INITIAL_PAGE = 0;

    private static final int INITIAL_PAGE_SIZE = 5;

    private static final int[] PAGE_SIZES = {5, 10};

    @Autowired
    ClientRepo clientrepository;

    @GetMapping("/")
    public ModelAndView homepage(@RequestParam("pageSize") Optional<Integer> pageSize,
                                 @RequestParam("page") Optional<Integer> page) {

        if (clientrepository.count() != 0) {
            //pass
        } else {
            addtorepository();
        }

        ModelAndView modelAndView = new ModelAndView("index");
        //
        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        // print repo
        System.out.println("here is client repo " + clientrepository.findAll());
        Page<Client> clientlist = clientrepository.findAll(new PageRequest(evalPage, evalPageSize));
        System.out.println("client list get total pages" + clientlist.getTotalPages() + "client list get number " + clientlist.getNumber());
        Pager pager = new Pager(clientlist.getTotalPages(), clientlist.getNumber(), BUTTONS_TO_SHOW);
        // add Client
        modelAndView.addObject("clientlist", clientlist);
        // evaluate page size
        modelAndView.addObject("selectedPageSize", evalPageSize);
        // add page sizes
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        // add pager
        modelAndView.addObject("pager", pager);
        return modelAndView;

    }

    public void addtorepository() {

        //below we are adding clients to our repository for the sake of this example
        Client widget = new Client();
        widget.setAddress("123 Fake Street");
        widget.setCurrentInvoice(10000);
        widget.setName("Widget Inc");

        clientrepository.save(widget);

        //next client
        Client foo = new Client();
        foo.setAddress("456 Attorney Drive");
        foo.setCurrentInvoice(20000);
        foo.setName("Foo LLP");

        clientrepository.save(foo);

        //next client
        Client bar = new Client();
        bar.setAddress("111 Bar Street");
        bar.setCurrentInvoice(30000);
        bar.setName("Bar and Food");
        clientrepository.save(bar);

        //next client
        Client dog = new Client();
        dog.setAddress("222 Dog Drive");
        dog.setCurrentInvoice(40000);
        dog.setName("Dog Food and Accessories");
        clientrepository.save(dog);

        //next client
        Client cat = new Client();
        cat.setAddress("333 Cat Court");
        cat.setCurrentInvoice(50000);
        cat.setName("Cat Food");
        clientrepository.save(cat);

        //next client
        Client hat = new Client();
        hat.setAddress("444 Hat Drive");
        hat.setCurrentInvoice(60000);
        hat.setName("The Hat Shop");
        clientrepository.save(hat);

        //next client
        Client hatB = new Client();
        hatB.setAddress("445 Hat Drive");
        hatB.setCurrentInvoice(60000);
        hatB.setName("The Hat Shop B");
        clientrepository.save(hatB);

        //next client
        Client hatC = new Client();
        hatC.setAddress("446 Hat Drive");
        hatC.setCurrentInvoice(60000);
        hatC.setName("The Hat Shop C");
        clientrepository.save(hatC);

        //next client
        Client hatD = new Client();
        hatD.setAddress("446 Hat Drive");
        hatD.setCurrentInvoice(60000);
        hatD.setName("The Hat Shop D");
        clientrepository.save(hatD);

        //next client
        Client hatE = new Client();
        hatE.setAddress("447 Hat Drive");
        hatE.setCurrentInvoice(60000);
        hatE.setName("The Hat Shop E");
        clientrepository.save(hatE);

        //next client
        Client hatF = new Client();
        hatF.setAddress("448 Hat Drive");
        hatF.setCurrentInvoice(60000);
        hatF.setName("The Hat Shop F");
        clientrepository.save(hatF);

    }
}
