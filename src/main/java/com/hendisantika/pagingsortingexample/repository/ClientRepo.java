package com.hendisantika.pagingsortingexample.repository;

import com.hendisantika.pagingsortingexample.model.Client;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : paging-sorting-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/01/18
 * Time: 17.59
 * To change this template use File | Settings | File Templates.
 */

@Repository
public interface ClientRepo extends PagingAndSortingRepository<Client, Long> {
}
