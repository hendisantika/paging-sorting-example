package com.hendisantika.pagingsortingexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingSortingExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagingSortingExampleApplication.class, args);
	}
}
