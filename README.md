# Paging and Sorting Example

#### Showing Paging and Sorting Example in Spring Boot Application

Run this by copy to your terminal :

`mvn clean spring-boot:run`

Open your browser : `http://localhost:8080/`

![Paging and Sorting](img/home.png "Paging & Sorting")